# Set 3

## Task 1
- Generated files:
    - data1 - 31
        - 15
        - 12
        - 10
    - data2 - 400
        - 200
        - 175
        - 150
    - data3 - 150
        - 75
        - 65
        - 50    

## Task 2
- search on unsorted data
```
10 items was searched. Min = 9, Max = 25, on an average 16.2
12 items was searched. Min = 5, Max = 25, on an average 16.6666666666667
15 items was searched. Min = 1, Max = 25, on an average 14.3333333333333
```
- search on sorted data
```
Array was sorted
10 items was searched. Min = 4, Max = 31, on an average 19.9
12 items was searched. Min = 4, Max = 28, on an average 19.5
15 items was searched. Min = 4, Max = 28, on an average 17.7333333333333
```
- binary search on sorted data  
```
Binary searching
10 items was searched. Min = 3, Max = 5, on an average 4.4
12 items was searched. Min = 3, Max = 5, on an average 3.91666666666667
15 items was searched. Min = 3, Max = 5, on an average 4.46666666666667
```

## Task 3

## Task 4, 5
- ordinary tree
```
10 items were searched. Min = 3, Max = 6, on an average 5
12 items were searched. Min = 3, Max = 6, on an average 5
15 items were searched. Min = 1, Max = 6, on an average 4.93333333333333
```
- perfectly ballanced tree
```
10 items were searched. Min = 1, Max = 5, on an average 3.5
12 items were searched. Min = 1, Max = 5, on an average 3.41666666666667
15 items were searched. Min = 1, Max = 5, on an average 3.73333333333333
```

## Task 6
- M = 2
```
150 items were searched. Memory access: Min = 2, Max = 10, on an average 7.68666666666667. Disk access: Min = 2, Max = 5, on an average 4.66
175 items were searched. Memory access: Min = 3, Max = 10, on an average 7.74857142857143. Disk access: Min = 2, Max = 5, on an average 4.69142857142857
200 items were searched. Memory access: Min = 2, Max = 10, on an average 7.4. Disk access: Min = 2, Max = 5, on an average 4.515
```
- M = 3
```
150 items were searched. Memory access: Min = 2, Max = 10, on an average 7.69333333333333. Disk access: Min = 2, Max = 4, on an average 3.76
175 items were searched. Memory access: Min = 2, Max = 10, on an average 7.62857142857143. Disk access: Min = 2, Max = 4, on an average 3.77142857142857
200 items were searched. Memory access: Min = 2, Max = 10, on an average 7.665. Disk access: Min = 2, Max = 4, on an average 3.805
```
- M = 6
```
150 items were searched. Memory access: Min = 1, Max = 10, on an average 7.48. Disk access: Min = 1, Max = 3, on an average 2.88666666666667
175 items were searched. Memory access: Min = 1, Max = 10, on an average 7.77714285714286. Disk access: Min = 1, Max = 3, on an average 2.94285714285714
200 items were searched. Memory access: Min = 1, Max = 10, on an average 7.52. Disk access: Min = 1, Max = 3, on an average 2.86
```
- M = 7
```
150 items were searched. Memory access: Min = 2, Max = 9, on an average 6.98666666666667. Disk access: Min = 1, Max = 3, on an average 2.82666666666667
175 items were searched. Memory access: Min = 1, Max = 9, on an average 7.31428571428571. Disk access: Min = 1, Max = 3, on an average 2.88571428571429
200 items were searched. Memory access: Min = 1, Max = 9, on an average 7.175. Disk access: Min = 1, Max = 3, on an average 2.805
```
- M = 8
```
150 items were searched. Memory access: Min = 2, Max = 9, on an average 7.76. Disk access: Min = 2, Max = 3, on an average 2.92666666666667
175 items were searched. Memory access: Min = 4, Max = 9, on an average 7.61142857142857. Disk access: Min = 2, Max = 3, on an average 2.89714285714286
200 items were searched. Memory access: Min = 4, Max = 9, on an average 7.71. Disk access: Min = 2, Max = 3, on an average 2.94
```
- M = 9
```
150 items were searched. Memory access: Min = 2, Max = 10, on an average 7.22. Disk access: Min = 2, Max = 3, on an average 2.88666666666667
175 items were searched. Memory access: Min = 1, Max = 10, on an average 7.51428571428571. Disk access: Min = 1, Max = 3, on an average 2.90857142857143
200 items were searched. Memory access: Min = 1, Max = 10, on an average 7.44. Disk access: Min = 1, Max = 3, on an average 2.915
```

## Taks 7
[Wolfram](www.wolframalpha.com)

## Taks 8, 9
- M = 2
```
150 items were searched. Memory access: Min = 6, Max = 12, on an average 8.57333333333333. Disk access: Min = 4, Max = 4, on an average 4
175 items were searched. Memory access: Min = 6, Max = 11, on an average 8.34857142857143. Disk access: Min = 4, Max = 4, on an average 4
200 items were searched. Memory access: Min = 6, Max = 12, on an average 8.455. Disk access: Min = 4, Max = 4, on an average 4
```
- M = 3
```
150 items were searched. Memory access: Min = 7, Max = 10, on an average 8.37333333333333. Disk access: Min = 4, Max = 4, on an average 4
175 items were searched. Memory access: Min = 7, Max = 10, on an average 8.48571428571429. Disk access: Min = 4, Max = 4, on an average 4
200 items were searched. Memory access: Min = 7, Max = 10, on an average 8.34. Disk access: Min = 4, Max = 4, on an average 4
```
- M = 6
```
150 items were searched. Memory access: Min = 6, Max = 10, on an average 7.89333333333333. Disk access: Min = 3, Max = 3, on an average 3
175 items were searched. Memory access: Min = 6, Max = 10, on an average 7.82857142857143. Disk access: Min = 3, Max = 3, on an average 3
200 items were searched. Memory access: Min = 6, Max = 10, on an average 7.995. Disk access: Min = 3, Max = 3, on an average 3
```
- M = 7
```
150 items were searched. Memory access: Min = 6, Max = 9, on an average 7.86666666666667. Disk access: Min = 3, Max = 3, on an average 3
175 items were searched. Memory access: Min = 5, Max = 9, on an average 7.81142857142857. Disk access: Min = 3, Max = 3, on an average 3
200 items were searched. Memory access: Min = 6, Max = 9, on an average 7.91. Disk access: Min = 3, Max = 3, on an average 3
```
- M = 8
```
150 items were searched. Memory access: Min = 5, Max = 10, on an average 8.14. Disk access: Min = 3, Max = 3, on an average 3
175 items were searched. Memory access: Min = 5, Max = 10, on an average 7.96571428571429. Disk access: Min = 3, Max = 3, on an average 3
200 items were searched. Memory access: Min = 5, Max = 10, on an average 7.925. Disk access: Min = 3, Max = 3, on an average 3
```
- M = 9
```
150 items were searched. Memory access: Min = 5, Max = 10, on an average 7.73333333333333. Disk access: Min = 3, Max = 3, on an average 3
175 items were searched. Memory access: Min = 5, Max = 10, on an average 7.86857142857143. Disk access: Min = 3, Max = 3, on an average 3
200 items were searched. Memory access: Min = 5, Max = 10, on an average 7.92. Disk access: Min = 3, Max = 3, on an average 3
```

## Taks 10
- ## Size = 195
- H1 L1
```
50 items were searched Min = 1, Max = 144, on an average 62.42
60 items were searched Min = 1, Max = 144, on an average 58.8
75 items were searched Min = 1, Max = 144, on an average 62.6933333333333
```
- H2 L1
```
50 items were searched Min = 1, Max = 6, on an average 1.7
60 items were searched Min = 1, Max = 6, on an average 1.51666666666667
75 items were searched Min = 1, Max = 6, on an average 1.64
```
- H3 L1
```
50 items were searched Min = 1, Max = 6, on an average 1.86
60 items were searched Min = 1, Max = 5, on an average 1.7
75 items were searched Min = 1, Max = 6, on an average 1.82666666666667
```
- H4 L1
```
50 items were searched Min = 1, Max = 18, on an average 1.96
60 items were searched Min = 1, Max = 18, on an average 3.41666666666667
75 items were searched Min = 1, Max = 18, on an average 2.57333333333333
```

- H2 L2
```
50 items were searched Min = 1, Max = 11, on an average 2.54
60 items were searched Min = 1, Max = 9, on an average 1.96666666666667
75 items were searched Min = 1, Max = 11, on an average 2.76
```
- H2 L3
```
50 items were searched Min = 1, Max = 12, on an average 2.34
60 items were searched Min = 1, Max = 12, on an average 2.6
75 items were searched Min = 1, Max = 12, on an average 2.68
```
- H2 L4
```
50 items were searched Min = 1, Max = 11, on an average 2.18
60 items were searched Min = 1, Max = 8, on an average 1.81666666666667
75 items were searched Min = 1, Max = 11, on an average 2.10666666666667
```

- ## Size = 250
- H2 L4
```
50 items were searched Min = 1, Max = 5, on an average 1.76
60 items were searched Min = 1, Max = 4, on an average 1.53333333333333
75 items were searched Min = 1, Max = 5, on an average 1.64
```

- ## Size = 220
- H2 L4
```
50 items were searched Min = 1, Max = 4, on an average 1.62
60 items were searched Min = 1, Max = 4, on an average 1.6
75 items were searched Min = 1, Max = 4, on an average 1.72
```