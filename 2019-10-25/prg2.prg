loop: POB extremum
inc1: POB array
      SOZ end
      ODE val
      SOZ haveExtremum  
continue1: POB one
inc2:      POB array
           ODE val
           SOM newExtremum   
continue2: POB one
           POB inc1
           DOD one
           LAD inc1
           POB inc2
           DOD one
           LAD inc2
           POB inc3
           DOD one
           LAD inc3
           POB i
           DOD one
           LAD i
           SOB loop
newExtremum: POB one
inc3:        POB array
             LAD val
             POB one
             LAD extremum
             POB i
             LAD label
             SOB continue2             
haveExtremum: POB extremum
              DOD one
              LAD extremum
              POB i
              LAD label
              SOB continue1   
one: RST 1
array: RST 3
       RST 2
       RST 3
       RST 11
       RST 1
       RST 2
       RST 3
       RST 1
       RST 0
       RST 0
extremum: RST 0
val: RST 999
label: RST 0
i: RST 0                    
end: POB extremum
     STP