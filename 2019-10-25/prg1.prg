loop: POB val_1
      ODE one
      SOM end
      LAD val_1
      POB prod
      DOD val_2
      LAD prod
      SOB loop
end: POB prod
     STP
prod: RST 0
val_1: RST 9       
val_2: RST 2
one: RST 1