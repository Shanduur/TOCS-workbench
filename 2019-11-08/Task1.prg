IN 1
LAD 1
ODE A_ZERO
ODE ONE
N: LAD N
J: LAD J
I: LAD I

POB N
LAD VAR_1
LAD VAR_2
LAD VAR_3
DOD A_ZERO
OUT 2
POB J
DOD A_ZERO
OUT 2
POB I
DOD A_ZERO
OUT 2
POB A_CR
OUT 2

ver:
        POB N
        LAD I
hor:
        POB A_DOT
        OUT 2
        POB I
        ODE ONE
        LAD I
        SOM ver_cr
        SOB hor
ver_cr:
       POB A_CR
       OUT 2
       POB J
       ODE ONE
       LAD J
       SOM END
       SOB ver
                        
END: stp

A_DOT:  RST '.'
A_STAR: RST '*'
ONE: RST 1

VAR_1: RST 0
VAR_2: RST 0 
VAR_3: RST 0

A_ZERO: RST 48
A_CR: RST 13

