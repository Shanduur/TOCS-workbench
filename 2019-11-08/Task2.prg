IN 1
LAD 1
ODE A_ZERO
ODE ONE
N: LAD N
J: LAD J
I: LAD I

POB J
LAD VAR_2

ver:
        POB N
        LAD I 
        POB J
        ODE VAR_2
        LAD VAR_1        
hor:
        POB VAR_1
        SOZ dot
        SOB star
dot:    
        POB A_DOT
        OUT 2
        POB I
        ODE ONE
        LAD I
        SOM ver_cr
        SOB hor
star:   POB VAR_1
        DOD ONE
        LAD VAR_1
        POB A_STAR
        OUT 2
        POB I
        ODE ONE
        LAD I
        SOM ver_cr
        SOB hor
        
ver_cr:
       POB A_CR
       OUT 2
       POB J
       ODE ONE
       LAD J
       SOM END
       SOB ver
                        
END: stp

A_DOT:  RST '.'
A_STAR: RST '*'
ONE: RST 1

VAR_1: RST 0
VAR_2: RST 0

A_ZERO: RST 48
A_CR: RST 13

