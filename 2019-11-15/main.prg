BEGIN:
      SOB PROGRAM 
      SOB INT1
      SOB INT2
      SOB INT3
      SOB INT4 

// Program procedure
PROGRAM:
        // Reading default sign from input
        IN 1
        LAD aSign
        POB aSign
        // loop showing default sign
        loop:
             OUT 2
             SOB loop
        STP
        
// Interrupt 1 procedure     
INT1:
     SETM lockOne
     LAD accBackupOne  
     // checking number of calls
     POB cntOne   
     ODE one
     LAD cntOne
     // if < 0 then finish program
     SOM endOne
     SOB printOne
     endOne:
            POB aOne
            OUT 2
            SOB KONIEC
     // printing number of interrupt
     printOne:
              POB ones
              LAD iOne
              oneLoop:
                      POB aOne
                      OUT 2
                      POB iOne
                      ODE one
                      LAD iOne
                      SOM resOne
                      SOB oneLoop
     resOne:
            POB accBackupOne
            SETM unlock
            RFI

// Interrupt 2 procedure
INT2:
     SETM lockTwo
     LAD accBackupTwo
     // checking number of calls
     POB cntTwo   
     ODE one
     LAD cntTwo
     // if < 0 then finish program
     SOM endTwo
     SOB printTwo
     endTwo: 
            POB aTwo
            OUT 2
            SOB KONIEC
     // printing number of interrupt
     printTwo:
              POB twos
              LAD iTwo
              twoLoop:
                      POB aTwo
                      OUT 2
                      POB iTwo
                      ODE one
                      LAD iTwo
                      SOM resTwo
                      SOB twoLoop
     resTwo:
            POB accBackupTwo
            SETM unlock
            RFI

// Interrupt 3 procedure
INT3:
     SETM lockThr
     LAD accBackupThr
     // checking number of calls
     POB cntThr   
     ODE one
     LAD cntThr
     // if < 0 then finish program
     SOM endThr
     SOB printThr
     endThr: 
            POB aThr
            OUT 2
            SOB KONIEC
     // printing number of interrupt
     printThr:
              POB threes
              LAD iThr
              thrLoop:
                      POB aThr
                      OUT 2
                      POB iThr
                      ODE one
                      LAD iThr
                      SOM resThr
                      SOB thrLoop
     resThr:
            POB accBackupThr
            SETM unlock
            RFI

// Interrupt 4 procedure
INT4:
     SETM lockFou
     LAD accBackupFou
     // checking number of calls
     POB cntFou   
     ODE one
     LAD cntFou
     // if < 0 then finish program
     SOM endFou
     SOB printFou
     endFou:
            POB aFou
            OUT 2
            SOB KONIEC
     // printing number of interrupt
     printFou:
              POB fours
              LAD iFou
              fouLoop:
                      POB aFou
                      OUT 2
                      POB iFou
                      ODE one
                      LAD iFou
                      SOM resFou
                      SOB fouLoop
     resFou:
            POB accBackupFou
            SETM unlock
            RFI

// Finishing the program
KONIEC:
       STP
   
// Basic Variables
zero: RST 0
one: RST 1 
two: RST 2
         
// Backups
accBackupOne:  RPA
accBackupTwo:  RPA
accBackupThr:  RPA
accBackupFou:  RPA

// numbers of showed numbers (whatever bruh)
ones:   RST 2
twos:   RST 4
threes: RST 6
fours:  RST 8

// ASCII chars
aSign: RPA
aOne: RST '1'
aTwo: RST '2'
aThr: RST '3'
aFou: RST '4'

// Locks for lower priority interrupts
unlock:  RST 0
lockOne: RST 15
lockTwo: RST 7
lockThr: RST 3
lockFou: RST 1

// Counters for knowing number of interrupt repetitions
iOne: RPA
iTwo: RPA
iThr: RPA
iFou: RPA
cntOne: RST 4
cntTwo: RST 4
cntThr: RST 4
cntFou: RST 4