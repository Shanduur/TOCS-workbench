BEGIN:
      SOB PROGRAM 
      SOB INT1
      SOB INT2
      SOB INT3
      SOB INT4 

// Program procedure
PROGRAM:
        // Reading default sign from input
        IN 1
        LAD aSign
        POB aSign
        // loop showing default sign
        loop:
             OUT 2
             SOB loop
        STP
        
// Interrupt 1 procedure     
INT1:
     SETM lockOne
     LAD accBackupOne  
     // checking number of calls
     POB cntOne   
     ODE one
     LAD cntOne
     // if < 0 then finish program
     SOM endOne
     SOB printOne
     endOne:
            POB aOne
            OUT 2
            SOB KONIEC
            POB accBackupOne
            SETM unlock
            RFI

// Interrupt 2 procedure
INT2:
     SETM lockTwo
     LAD accBackupTwo
     // checking number of calls
     POB cntTwo   
     ODE one
     LAD cntTwo
     // if < 0 then finish program
     SOM endTwo
     SOB printTwo
     endTwo: 
            POB aTwo
            OUT 2
            SOB KONIEC
            POB accBackupTwo
            SETM unlock
            RFI

// Interrupt 3 procedure
INT3:
     SETM lockThr
     LAD accBackupThr
     // checking number of calls
     POB cntThr   
     ODE one
     LAD cntThr
     // if < 0 then finish program
     SOM endThr
     SOB printThr
     endThr: 
            POB aThr
            OUT 2
            SOB KONIEC
            POB accBackupThr
            SETM unlock
            RFI

// Interrupt 4 procedure
INT4:
     SETM lockFou
     LAD accBackupFou
     // checking number of calls
     POB cntFou   
     ODE one
     LAD cntFou
     // if < 0 then finish program
     SOM endFou
     SOB printFou
     endFou:
            POB aFou
            OUT 2
            SOB KONIEC
            POB accBackupFou
            SETM unlock
            RFI

// Finishing the program
KONIEC:
       STP
   
// Basic Variables
zero: RST 0
one: RST 1 
two: RST 2
         
// Backups
accBackupOne:  RPA
accBackupTwo:  RPA
accBackupThr:  RPA
accBackupFou:  RPA

// ASCII chars
aSign: RPA

// Locks for lower priority interrupts
unlock:  RST 0
lockOne: RST 15
lockTwo: RST 7
lockThr: RST 3
lockFou: RST 1

// Counters for knowing number of interrupt repetitions
cntOne: RST 4
cntTwo: RST 4
cntThr: RST 4
cntFou: RST 4